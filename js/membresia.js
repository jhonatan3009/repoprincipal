$(function () {

    $('#membresias').on('show.bs.modal', function (e) {
        console.log('el modal se esta abriendo');
        $('#openModal').removeClass('btn-primary');
        $('#openModal').addClass('btn-secondary');
        
    })

    $('#membresias').on('shown.bs.modal', function (e) {
        console.log('el modal se abrio');
    })

    $('#membresias').on('hide.bs.modal', function (e) {
        console.log('el modal se esta cerrando');

        $('#openModal').removeClass('btn-secondary');
        $('#openModal').addClass('btn-primary');
    })

    $('#membresias').on('hidden.bs.modal', function (e) {
        console.log('el modal se cerro');
    })

});